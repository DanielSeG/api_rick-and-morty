import { Injectable } from '@angular/core';
import{HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class RymService {
  public URI = 'https://rickandmortyapi.com/api/character'

                                                    

  constructor( private _http : HttpClient) { }  //Esta es una instancia con el _http

  getPersonajes(){ //metodo get
    return this._http.get(this.URI)
    //Segunda forma
    //const url = '${this.URI}/character'
    return this._http.get(this.URI)
  }
}
