import { Component, OnInit } from '@angular/core';
import { RymService } from '../RickAndMorrty/rym.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})

export class HomeComponent implements OnInit {
  public personajes : any [] = [];

  constructor( private _rym : RymService ) {}


  ngOnInit(): void {
    this.muestraPersonajes()
  }

  muestraPersonajes() {
    this._rym.getPersonajes()
      .subscribe((data:any) => {
        console.log(data);
        
      })

  }


}
